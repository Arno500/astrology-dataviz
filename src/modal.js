import { emptyNode } from "./utils";
import { sortedBySigns, convertToSentence } from "./data";
import pictos from "../ressources/pictos/*.svg";
let modal = document.querySelector("modal");
const signsData = [
  {
    name: "Capricorne",
    img: require("../ressources/signs-modal/picto-capricorne.svg")
  },
  {
    name: "Sagittaire",
    img: require("../ressources/signs-modal/picto-sagittaire.svg")
  },
  {
    name: "Gémeaux",
    img: require("../ressources/signs-modal/picto-gemeaux.svg")
  },
  {
    name: "Balance",
    img: require("../ressources/signs-modal/picto-balance.svg")
  },
  {
    name: "Bélier",
    img: require("../ressources/signs-modal/picto-belier.svg")
  },
  {
    name: "Cancer",
    img: require("../ressources/signs-modal/picto-cancer.svg")
  },
  { name: "Lion", img: require("../ressources/signs-modal/picto-lion.svg") },
  {
    name: "Poisson",
    img: require("../ressources/signs-modal/picto-poisson.svg")
  },
  {
    name: "Scorpion",
    img: require("../ressources/signs-modal/picto-scorpion.svg")
  },
  {
    name: "Taureau",
    img: require("../ressources/signs-modal/picto-taureau.svg")
  },
  {
    name: "Verseau",
    img: require("../ressources/signs-modal/picto-verseau.svg")
  },
  { name: "Vierge", img: require("../ressources/signs-modal/picto-vierge.svg") }
];
export function openModalFor(signName, personName) {
  modal = emptyNode(modal);
  const signInfos = signsData.find(elm => elm.name === signName);
  const stats = sortedBySigns.sortedStats[signName];
  stats.length = 4;
  modal.classList.add("open");
  modal.insertAdjacentHTML(
    "beforeend",
    `
        <div class="modal-container">
        <img class="picto-sign" src="${signInfos.img}" alt=${signInfos.name}>
        <div class="modal-text">
            <p class="intro-text">${personName} est ${signName}. Les ${signName +
      (signName.slice(-1) !== "x" ? "s" : "")} : </p>
      <div class="stats">
      ${stats
        .map(
          elm =>
            `<p><img src="${pictos[elm.id]}">${convertToSentence(
              elm.value,
              elm.id
            )}</p>`
        )
        .join("")}
      </div>
            </div>
        </div>
    `
  );
  modal.addEventListener("click", closeModal);
}

export function closeModal() {
  modal.classList.remove("open");
}
