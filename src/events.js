import { compatWheel } from "./utils";
import { zoomBackground, camera } from "./background";
// import { throttle } from "lodash-es";

export function setupEvents() {
  compatWheel();
  let gestureStartScale;
  const background = document.querySelector("background");

  background.addEventListener("gesturestart", function(e) {
    e.preventDefault();
    gestureStartScale = camera.zoom;
  });

  background.addEventListener("gesturechange", function(e) {
    e.preventDefault();
    let scale = gestureStartScale * e.scale;

    zoomBackground(scale);
  });

  background.addEventListener("gestureend", function(e) {
    e.preventDefault();
  });
  addWheelListener(
    document.body,
    e => {
      e.preventDefault();
      let scale = camera.zoom;
      if (e.ctrlKey) {
        scale += e.deltaY * 0.25;
      } else {
        scale += e.deltaY * 0.1;
      }
      zoomBackground(scale);
    },
    { passive: false }
  );

  // Drag and drop transform

  let isDown = false;

  document.addEventListener(
    "mousedown",
    function() {
      isDown = true;
    },
    true
  );

  document.addEventListener(
    "mouseup",
    function() {
      isDown = false;
    },
    true
  );

  //   let throttledCameraMove = throttle((x, y) => camera.move(x, y), 1 / 30);

  background.addEventListener(
    "mousemove",
    function(e) {
      e.preventDefault();
      if (isDown) {
        camera.move(e.movementX, e.movementY);
        // throttledCameraMove(e.movementX, e.movementY);
      }
    },
    true
  );
}
