import * as d3 from "d3";
import { groupBy, cloneDeep, meanBy, countBy, forIn, orderBy } from "lodash-es";
import { convertObjToArray, removeFirstCaps } from "./utils";

export let convertedData;
export let sortedBySigns;
export let globalStats;
export let stubborn;
let d3Data;
export const signsData = [
  { name: "Capricorne", img: require("../ressources/signs/capricorne.svg") },
  { name: "Sagittaire", img: require("../ressources/signs/sagittaire.svg") },
  { name: "Gémeaux", img: require("../ressources/signs/gemeaux.svg") },
  { name: "Balance", img: require("../ressources/signs/balance.svg") },
  { name: "Bélier", img: require("../ressources/signs/bélier.svg") },
  { name: "Cancer", img: require("../ressources/signs/cancer.svg") },
  { name: "Lion", img: require("../ressources/signs/lion.svg") },
  { name: "Poisson", img: require("../ressources/signs/poisson.svg") },
  { name: "Scorpion", img: require("../ressources/signs/scorpion.svg") },
  { name: "Taureau", img: require("../ressources/signs/taureau.svg") },
  { name: "Verseau", img: require("../ressources/signs/verseau.svg") },
  { name: "Vierge", img: require("../ressources/signs/vierge.svg") }
];
const scheme = {
  astro: {
    type: "value",
    perSign: false
  },
  stubborn: {
    type: "binary"
    //   zero: 0,
    //   one: 1
  },
  humor: {
    type: "integer",
    min: 1,
    max: 5
  },
  lucky: {
    type: "integer",
    min: 1,
    max: 5
  },
  transitTime: {
    type: "integer",
    min: 1,
    max: 10
  },
  pantsColor: {
    type: "value"
  },
  glasses: {
    type: "value"
  },
  shirt: {
    type: "value"
  },
  restaurant: {
    type: "binary"
    //   zero: "Non",
    //   one: "Oui"
  },
  goOut: {
    type: "integer",
    min: 1,
    max: 5
  },
  lostSunglasses: {
    type: "binary"
    //   zero: "Non",
    //   one: "Oui"
  },
  computer: {
    type: "binary"
    //   zero: "Mac",
    //   one: "PC"
  },
  dayNight: {
    type: "binary"
    //   zero: "Je vis le jour",
    //   one: "Je vis la nuit"
  }
};
export function prepareData() {
  return new Promise(resolve => {
    d3Data = d3.csv(require("../ressources/survey.csv")).then(data => {
      let untreatedData = cloneDeep(data);
      convertArray(untreatedData, "computer", convertBinary, ["Mac", "PC"]);
      convertArray(untreatedData, "restaurant", convertBinary, ["Non", "Oui"]);
      convertArray(untreatedData, "lostSunglasses", convertBinary, [
        "Non",
        "Oui"
      ]);
      convertArray(untreatedData, "dayNight", convertBinary, [
        "Je vis la nuit",
        "Je vis le jour"
      ]);
      // convertArray(untreatedData, "glasses", convertGlassesShape);
      //   convertArray(untreatedData, "shirt", convertShirtCanvas);
      //   convertArray(untreatedData, "pantsColor", convertPantsColor);
      convertArray(untreatedData, "stubborn", parseInt);
      convertArray(untreatedData, "transitTime", parseInt);
      convertArray(untreatedData, "lucky", parseInt);
      convertArray(untreatedData, "goOut", parseInt);
      convertArray(untreatedData, "humor", parseInt);

      globalStats = createStatsFromScheme(untreatedData, scheme);
      sortedBySigns = groupBy(untreatedData, "astro");
      forIn(sortedBySigns, (value, key) => {
        if (!sortedBySigns.stats) sortedBySigns.stats = {};
        sortedBySigns.stats[key] = createStatsFromScheme(value, scheme);
      });
      forIn(sortedBySigns.stats, value => findInterestingValues(value, scheme));
      cleanPerSign(sortedBySigns.stats, scheme);
      sortedBySigns.sortedStats = getInterestingStats(sortedBySigns.stats, 4);
      console.log(JSON.stringify(sortedBySigns.sortedStats));

      console.log(globalStats);
      resolve();
    });
  });
}

// function convertPantsColor(color) {
//   let pant = " ont un pantalon ";
//   return pant + color;
// //   switch (color) {
// //     case "Noir":
// //       return pant + "noir";
// //     case "Bleu":
// //       return pant + "bleu";
// //     case "Rouge":
// //       return 2;
// //     case "Blanc":
// //       return 3;
// //     case "Jaune":
// //       return 4;
// //     case "Rose":
// //       return 5;
// //     case "Vert":
// //       return 6;
// //     default:
// //       throw new Error("Invalid pants color: ", JSON.stringify(color));
// //   }
// }

function convertGlassesShape(value) {
  switch (value) {
    case "J'ai des yeux forts (JCVD)":
      return 0;
    case "Moi, je triche, j'ai des lentilles !":
      return 1;
    case "Losange":
      return 2;
    case "Rondes":
      return 3;
    case "Carrées":
      return 4;
    default:
      throw new Error("Invalid glasses shapes: ", JSON.stringify(value));
  }
}

// function convertShirtCanvas(value) {
//   switch (value) {
//     case "Unie":
//       return 0;
//     case "À carreaux":
//       return 1;
//     case "À pois":
//       return 2;
//     case "À fleurs":
//       return 3;
//     case "Hawaïenne":
//       return 4;
//     case "À rayures":
//       return 5;
//     default:
//       throw new Error("Invalid shirt shape: " + value);
//   }
// }

function convertBinary(value, zeroValue, oneValue) {
  switch (value) {
    case oneValue:
      return 1;
    case zeroValue:
      return 0;
    default:
      throw new Error("Unhandeld value");
  }
}

function average(values) {
  const sum = values.reduce((elm, acc) => elm + acc, 0);
  return sum / values.length;
}

function averageRounded(values) {
  return Math.round(average(values));
}

function convertArray(array, keyToConvert, conversionFunction, args) {
  for (let i = 0; i < array.length; i++) {
    const elm = array[i];
    elm[keyToConvert] = conversionFunction(elm[keyToConvert], ...(args || []));
  }
}

function createStatsFromScheme(data, scheme) {
  let returnObject = {};
  forIn(scheme, (value, key) => {
    let entryData = {};
    switch (value.type) {
      case "binary":
        entryData.count = countBy(data, elm => elm[key]);
        entryData.average = meanBy(data, elm => elm[key]);
        break;
      case "integer":
        entryData.count = countBy(data, elm => elm[key]);
        entryData.average = meanBy(data, elm => elm[key]);
        entryData.proportions = getProportionsFromObj(
          entryData.count,
          data.length
        );
        break;
      case "value":
        entryData.count = countBy(data, elm => elm[key]);
        entryData.proportions = getProportionsFromObj(
          entryData.count,
          data.length
        );
        break;
    }
    returnObject[key] = entryData;
  });
  return returnObject;
}

function getProportionsFromObj(values, length) {
  let returnObject = {};
  forIn(values, (value, key) => {
    returnObject[key] = value / length;
  });
  return returnObject;
}

function findInterestingValues(values, scheme) {
  forIn(scheme, (value, key) => {
    switch (value.type) {
      case "binary":
        values[key].interestScore = Math.abs(values[key].average - 0.5) * 2;
        break;
      case "value":
        if (value.perSign !== false) {
          values[key].sortedProportions = orderBy(
            convertObjToArray(values[key].proportions),
            "value",
            "desc"
          );
          values[key].interestScore =
            Math.abs(values[key].sortedProportions[0].value - 0.5) * 2;
        }
        break;
      case "integer":
        let globalAverage = globalStats[key].average / value.max;
        let globalAverageScore = Math.abs(globalAverage - 0.5);
        let localAverage = Math.abs(values[key].average / value.max - 0.5);
        values[key].interestScore = globalAverageScore + localAverage;
        break;
    }
  });
}

function cleanPerSign(stats, scheme) {
  forIn(stats, sign => {
    forIn(scheme, (value, key) => {
      if (value.perSign === false) {
        delete sign[key];
      }
    });
  });
}

function getInterestingStats(stats, number) {
  let returnObject = {};
  forIn(stats, (value, key) => {
    const statsArray = convertObjToArray(value);
    const sortedStats = orderBy(statsArray, "value.interestScore", "desc");
    if (number) sortedStats.length = number;
    returnObject[key] = sortedStats;
  });
  return returnObject;
}

export function convertToSentence(value, name) {
  let average;
  let majorObj;
  let textToShow;
  switch (name) {
    case "stubborn":
      return `${Math.round(value.average * 100)}% sont têtus`;
    case "humor":
      average = Math.round(value.average);
      switch (average) {
        case 1:
          return "Ils ne se sentent vraiment pas bien";
        case 2:
          return "Ils ne vont pas très très bien en ce moment";
        case 3:
          return "Leur humeur est stable";
        case 4:
          return "Ils sont plutôt contents en général";
        case 5:
          return "Ils sont super contents !";
      }
    case "lucky":
      average = Math.round(value.average);
      switch (average) {
        case 1:
          return "Sont carrément malchanceux";
        case 2:
          return "La chance ne leur sourit pas";
        case 3:
          return "Leur chance dépend des jours";
        case 4:
          return "En général, ils sont chanceux";
        case 5:
          return "La chance est leur point fort";
      }
    //   return `Chance moyenne : ${Math.round(value.average)} / 5`;
    case "transitTime":
      return `Temps de trajet moyen : ${Math.round(
        value.average * 10
      )} minutes`;
    case "pantsColor":
      majorObj = value.sortedProportions[0];
      return `${Math.round(
        majorObj.value * 100
      )}% mettent des pantalons ${removeFirstCaps(majorObj.id)}`;
    case "glasses":
      majorObj = value.sortedProportions[0];
      if (majorObj.id === "J'ai des yeux forts (JCVD)") {
        textToShow = "n'ont pas de lunettes";
      } else if (majorObj.id === "Moi, je triche, j'ai des lentilles !") {
        textToShow = "ont des lentilles";
      } else {
        textToShow = `ont des lunettes ${removeFirstCaps(majorObj.id)}`;
      }
      return `${Math.round(majorObj.value * 100)}% ${textToShow}`;
    case "shirt":
      majorObj = value.sortedProportions[0];
      return `${Math.round(
        majorObj.value * 100
      )}% préfèrent les chemises ${removeFirstCaps(majorObj.id)}`;
    case "restaurant":
      average = value.average;
      if (Math.round(average) === 1) {
        return `${Math.round(average * 100)}% mangent à la cantine`;
      } else {
        return `${Math.round(
          Math.abs(average - 1 * 100)
        )}% ne mangent pas à la cantine`;
      }
    case "goOut":
      average = Math.round(value.average);
      switch (average) {
        case 1:
          return "N'aiment pas du tout sortir";
        case 2:
          return "N'aiment pas trop sortir";
        case 3:
          return "Sortent de temps en temps";
        case 4:
          return "Sortent tout le temps";
        case 5:
          return "Vont sûrement boire un verre juste après cette présentation";
      }
    case "lostSunglasses":
      average = value.average;
      if (Math.round(average) === 1) {
        return `${Math.round(average * 100)}% perdent souvent leurs lunettes`;
      } else {
        return `${Math.round(
          Math.abs(average - 1 * 100)
        )}% ne perdent pas leurs lunettes`;
      }
    case "computer":
      average = value.average;
      if (Math.round(average) === 1) {
        return `${Math.round(average * 100)}% ont un PC`;
      } else {
        return `${Math.round(Math.abs(average - 1 * 100))}% ont un Mac`;
      }
    case "dayNight":
      average = value.average;
      if (Math.round(average) === 1) {
        return `${Math.round(average * 100)}% sont plutôt de jour`;
      } else {
        return `${Math.round(
          Math.abs(average - 1 * 100)
        )}% sont des oiseaux de nuit`;
      }
    default:
      return "";
  }
}
