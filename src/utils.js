export function compatWheel() {
  var prefix = "",
    _addEventListener,
    support;

  // detect event model
  if (window.addEventListener) {
    _addEventListener = "addEventListener";
  } else {
    _addEventListener = "attachEvent";
    prefix = "on";
  }

  // detect available wheel event
  support =
    "onwheel" in document.createElement("div")
      ? "wheel" // Modern browsers support "wheel"
      : document.onmousewheel !== undefined
      ? "mousewheel" // Webkit and IE support at least "mousewheel"
      : "DOMMouseScroll"; // let's assume that remaining browsers are older Firefox

  window.addWheelListener = function(elem, callback, options) {
    _addWheelListener(elem, support, callback, options);

    // handle MozMousePixelScroll in older Firefox
    if (support == "DOMMouseScroll") {
      _addWheelListener(elem, "MozMousePixelScroll", callback, options);
    }
  };

  function _addWheelListener(elem, eventName, callback, options) {
    elem[_addEventListener](
      prefix + eventName,
      support == "wheel"
        ? callback
        : function(originalEvent) {
            !originalEvent && (originalEvent = window.event);

            // create a normalized event object
            var event = {
              // keep a ref to the original event object
              originalEvent: originalEvent,
              target: originalEvent.target || originalEvent.srcElement,
              type: "wheel",
              deltaMode: originalEvent.type == "MozMousePixelScroll" ? 0 : 1,
              deltaX: 0,
              deltaY: 0,
              deltaZ: 0,
              preventDefault: function() {
                originalEvent.preventDefault
                  ? originalEvent.preventDefault()
                  : (originalEvent.returnValue = false);
              }
            };

            // calculate deltaY (and deltaX) according to the event
            if (support == "mousewheel") {
              event.deltaY = (-1 / 40) * originalEvent.wheelDelta;
              // Webkit also support wheelDeltaX
              originalEvent.wheelDeltaX &&
                (event.deltaX = (-1 / 40) * originalEvent.wheelDeltaX);
            } else {
              event.deltaY = originalEvent.deltaY || originalEvent.detail;
            }

            // it's time to fire the callback
            return callback(event);
          },
      options
    );
  }
}

export function random(max) {
  return Math.round(Math.random() * max);
}

export function emptyNode(node) {
  const cNode = node.cloneNode(false);
  node.parentNode.replaceChild(cNode, node);
  return cNode;
}

export function map(x, in_min, in_max, out_min, out_max) {
  return ((x - in_min) * (out_max - out_min)) / (in_max - in_min) + out_min;
}

export function convertObjToArray(obj) {
  return Object.keys(obj).map(id => ({
    id,
    value: obj[id]
  }));
}

export function removeFirstCaps(string) {
  return string.charAt(0).toLowerCase() + string.slice(1);
}
