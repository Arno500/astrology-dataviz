import { random, emptyNode, map } from "./utils";
import { throttle, debounce } from "lodash-es";
import PoissonDiskSampling from "poisson-disk-sampling";
import faces from "../ressources/faces/*.svg";
import { signsData, prepareData, sortedBySigns } from "./data";
import { openModalFor } from "./modal";

export async function initBackground() {
  generateStars();
  zoomBackground(100);
  await prepareData();
  generateSigns();
  camera.move(
    -(clientSize.x * CANVAS_COEF) / 2,
    -(clientSize.y * CANVAS_COEF) / 2
  );
}

let stars = [];
let signs = [];
let background = document.querySelector("background");
let starsContainer = document.querySelector("stars");
let signsContainer = document.querySelector("signs");
let rootElement = document.documentElement;
let starsImages = [
  require("../ressources/etoile-1.svg"),
  require("../ressources/etoile-2.svg"),
  require("../ressources/etoile-3.svg"),
  require("../ressources/etoile-4.svg")
];

let clientSize = {
  x: background.clientWidth,
  y: background.clientHeight
};

window.onresize = debounce(unDebouncedResize, 200);

function unDebouncedResize() {
  clientSize = {
    x: background.clientWidth,
    y: background.clientHeight
  };
  starsContainer = emptyNode(starsContainer);
  signsContainer = emptyNode(signsContainer);
  camera.x = 0;
  camera.y = 0;
  initBackground();
}

const CANVAS_COEF = 1.5;

const DEPTH = 3000;
const STARS_COUNT = 800;
const SIGN_Z = 1000;
const DISTANCE_FROM_SIGN_CENTER = 150;
const Z_STAR_RANDOMIZER = 100;
const Z_SIGN_RANDOMIZER = 100;

class Camera {
  constructor() {
    this.zoom = 80;
    this.x = 0;
    this.y = 0;
    this.z = 0;
    background.style.perspective = "300px";
  }
  setZoom(level) {
    this.zoom = level;
    this.z = Math.pow(camera.zoom, 1.5);
    rootElement.style.setProperty("--camera-z", -this.z);
  }
  move(x, y) {
    this.x -= x * Math.log(this.zoom) * 0.1;
    this.y -= y * Math.log(this.zoom) * 0.1;

    rootElement.style.setProperty("--camera-x", -this.x);
    rootElement.style.setProperty("--camera-y", -this.y);
  }
}

export const camera = new Camera();

class Star {
  constructor(x, y, z, scale, opacity, img) {
    this.x = x;
    this.y = y;
    this.z = z - z / 2;
    this.scale = scale || 1;
    this.zIndex = this.z;
    this.node = document.createElement("img");
    this.node.src =
      img || starsImages[Math.round(Math.random() * (starsImages.length - 1))];
    this.node.style.transform = `matrix3d(1,0,0,0,0,1,0,0,0,0,1,0,
      calc(${this.x} + var(--camera-x)),calc(${this.y} + var(--camera-y)),calc(${this.z} + var(--camera-z)),1) scale(${this.scale})`;
    this.node.style.zIndex = this.zIndex;
    if (img) this.node.classList.add("face");
    this.setOpacity(opacity || Math.random() * 0.6);
    starsContainer.insertAdjacentElement("beforeend", this.node);
  }
  setOpacity(opacity) {
    this.opacity = opacity;
    this.node.style.opacity = String(this.opacity);
  }
  addEventListenener(callback) {
    this.node.addEventListener("click", callback);
  }
}

function generateStars() {
  background.style.perspectiveOrigin = `${clientSize.x / 2}px ${clientSize.y /
    2}px`;
  for (let i = 0; i < STARS_COUNT; i++) {
    stars.push(
      new Star(
        random(clientSize.x * CANVAS_COEF * 2),
        random(clientSize.y * CANVAS_COEF * 2 - clientSize.y),
        random(DEPTH)
      )
    );
  }
}

class Sign {
  constructor(x, y, signData) {
    this.x = x;
    this.y = y;
    this.z =
      SIGN_Z + (Math.random() * Z_SIGN_RANDOMIZER - Z_SIGN_RANDOMIZER / 2);
    this.name = signData.name;
    this.opacity = 0.8;
    this.stars = [];
    this.containerNode = document.createElement("sign");
    this.node = document.createElement("img");
    this.node.src = signData.img;
    this.node.style.transform = `matrix3d(1,0,0,0,0,1,0,0,0,0,1,0,
      calc(${this.x} + var(--camera-x)),calc(${this.y} + var(--camera-y)),calc(${this.z} + var(--camera-z)),1)`;
    this.node.style.opacity = this.opacity;
    this.containerNode.insertAdjacentElement("beforeend", this.node);
    signsContainer.insertAdjacentElement("beforeend", this.containerNode);
    this.generateSelfStars();
  }
  setOpacity(zoom) {
    this.opacity = Math.min(map(zoom, 77, 97, 0.01, 0.8), 0.8);
    this.node.style.opacity = this.opacity;
    this.stars.forEach(elm => {
      elm.setOpacity(Math.max(map(zoom, 97, 77, 0.2, 0.8), 0.2));
    });
  }
  generateSelfStars() {
    let pds = new PoissonDiskSampling(
      [DISTANCE_FROM_SIGN_CENTER, DISTANCE_FROM_SIGN_CENTER],
      40,
      70
    );
    pds.addPoint(DISTANCE_FROM_SIGN_CENTER / 2, DISTANCE_FROM_SIGN_CENTER / 2);
    let posArray = pds.fill();
    for (let i = 0; i < sortedBySigns[this.name].length; i++) {
      const star = new Star(
        this.x + posArray[i][0],
        this.y + posArray[i][1],
        SIGN_Z -
          50 +
          (Math.random() * Z_STAR_RANDOMIZER - Z_STAR_RANDOMIZER / 2),
        1,
        0.2,
        faces[sortedBySigns[this.name][i].name]
      );
      star.addEventListenener(() =>
        openModalFor(this.name, sortedBySigns[this.name][i].name)
      );
      this.stars.push(star);
    }
  }
}
function generateSigns() {
  const x = clientSize.x * CANVAS_COEF;
  const y = clientSize.y * CANVAS_COEF;
  let pds = new PoissonDiskSampling([x, y], 250, 500, 10);
  pds.addPoint(x / 2, y / 2);
  let posArray = pds.fill();
  for (let i = 0; i < signsData.length; i++) {
    signs.push(new Sign(posArray[i][0], posArray[i][1], signsData[i]));
  }
}

function unThrottledZoomBackground(scale) {
  camera.setZoom(scale);
  signs.forEach(sign => {
    sign.setOpacity(camera.zoom);
  });
}

export const zoomBackground = throttle(unThrottledZoomBackground, 1 / 60);
